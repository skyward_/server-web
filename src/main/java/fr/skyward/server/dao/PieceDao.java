package fr.skyward.server.dao;

import fr.skyward.server.model.Piece;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PieceDao extends CrudRepository<Piece, Integer> {

}
