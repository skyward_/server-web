package fr.skyward.server.dao;

import fr.skyward.server.model.Rank;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RankDao extends CrudRepository<Rank, Integer> {
    Rank findByName(String name);
}
