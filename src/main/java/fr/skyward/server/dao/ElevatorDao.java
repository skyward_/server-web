package fr.skyward.server.dao;

import fr.skyward.server.model.Elevator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElevatorDao extends CrudRepository<Elevator, Integer> {

}
