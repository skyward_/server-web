package fr.skyward.server.dao;

import fr.skyward.server.model.Elevator;
import fr.skyward.server.model.Maintenance;
import fr.skyward.server.model.MaintenanceType;
import fr.skyward.server.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaintenanceDao extends CrudRepository<Maintenance, Integer> {

    Iterable<Maintenance> findByMaintenanceTypeAndEndedFalse(MaintenanceType maintenanceType);

    Iterable<Maintenance> findByMaintenanceTypeId(int maintenanceType_id);

    Iterable<Maintenance> findTop10ByElevatorOrderByDate(Elevator elevator);

    Iterable<Maintenance> findByTechnicianAndEndedFalse(User technician);

    Iterable<Maintenance> findByTechnician(User technician);

    Iterable<Maintenance> findByEndedFalse();

    @Modifying
    @Query("UPDATE Maintenance m SET m.technician = ?1 WHERE m.id = ?2")
    void updateUserOnMaintenance(int userId, int maintenanceId);

    @Modifying
    @Query("UPDATE Maintenance m SET m.technician = null WHERE m.id = ?1")
    void removeUserOnMaintenance(int maintenanceId);
}
