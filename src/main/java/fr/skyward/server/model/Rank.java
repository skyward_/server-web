package fr.skyward.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Rank {

    public Rank(String name) {
        this.name = name;
    }

    public Rank() {
    }

    @Id
    @GeneratedValue
    @Column(name = "rankId", columnDefinition = "serial")
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
