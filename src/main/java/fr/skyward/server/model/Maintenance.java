package fr.skyward.server.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Maintenance {

    @Id
    @GeneratedValue
    @Column(name = "maintenanceId", columnDefinition = "serial")
    private int id;

    @OneToOne
    @JoinColumn(name = "elevatorId")
    private Elevator elevator;

    @OneToOne
    @JoinColumn(name = "userId")
    private User technician;

    @OneToOne
    @JoinColumn(name = "maintenanceTypeId")
    private MaintenanceType maintenanceType;

    private String title;
    private String comment;
    private Date date;

    @OneToMany
    private Set<Piece> pieces;

    private String commentTechnician;
    private Date dateEnded;
    private boolean ended;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Elevator getElevator() {
        return elevator;
    }

    public void setElevator(Elevator elevator) {
        this.elevator = elevator;
    }

    public User getTechnician() {
        return technician;
    }

    public void setTechnician(User user) {
        this.technician = user;
    }

    public MaintenanceType getMaintenanceType() {
        return maintenanceType;
    }

    public void setMaintenanceType(MaintenanceType maintenanceType) {
        this.maintenanceType = maintenanceType;
    }

    public Set<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(Set<Piece> pieces) {
        this.pieces = pieces;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public Date getDateEnded() {
        return dateEnded;
    }

    public void setDateEnded(Date dateEnded) {
        this.dateEnded = dateEnded;
    }

    public String getCommentTechnician() {
        return commentTechnician;
    }

    public void setCommentTechnician(String commentTechnician) {
        this.commentTechnician = commentTechnician;
    }
}
