package fr.skyward.server.model;

import javax.persistence.*;

@Entity
public class Elevator {

    @Id
    @GeneratedValue
    @Column(name = "elevatorId", columnDefinition = "serial")
    private int id;

    @OneToOne
    @JoinColumn(name = "elevatorTypeId")
    private ElevatorType type;

    private String address;

    private String label;
    private String reference;

    private int maxWeight;
    private int amountFloor;

    private float width;
    private float length;
    private float weight;

    public Elevator(int id) {
        this.id = id;
    }

    public Elevator() {
    }

    @Override
    public String toString() {
        return "Elevator{" +
                "id=" + id +
                ", type=" + type +
                ", address='" + address + '\'' +
                ", label='" + label + '\'' +
                ", reference='" + reference + '\'' +
                ", maxWeight=" + maxWeight +
                ", amountFloor=" + amountFloor +
                ", width=" + width +
                ", length=" + length +
                ", weight=" + weight +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ElevatorType getType() {
        return type;
    }

    public void setType(ElevatorType type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public int getAmountFloor() {
        return amountFloor;
    }

    public void setAmountFloor(int amountFloor) {
        this.amountFloor = amountFloor;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
