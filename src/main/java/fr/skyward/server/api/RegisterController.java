package fr.skyward.server.api;

import fr.skyward.server.model.User;
import fr.skyward.server.service.RankService;
import fr.skyward.server.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "register")
public class RegisterController {

    private BCryptPasswordEncoder bCrypt;

    private final RegisterService registerService;
    private final RankService rankService;

    @Autowired
    public RegisterController(RegisterService registerService, BCryptPasswordEncoder bCrypt, RankService rankService) {
        this.registerService = registerService;
        this.bCrypt = bCrypt;
        this.rankService = rankService;
    }

    @GetMapping
    public Iterable<User> getUsers() {
        return registerService.getUsers();
    }

    @PostMapping("/{rank}")
    public User addUser(User user, @PathVariable String rank) {
        user.setPassword(this.bCrypt.encode(user.getPassword()));
        user.setRank(rankService.getRankByName(rank));

        return registerService.addUser(user);
    }
}
