package fr.skyward.server.api;

import fr.skyward.server.model.User;
import fr.skyward.server.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "login")
public class LoginController {


    private BCryptPasswordEncoder bCrypt;

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService, BCryptPasswordEncoder bCrypt) {
        this.loginService = loginService;
        this.bCrypt = bCrypt;
    }

    @GetMapping
    public Iterable<User> getUsers() {
        return loginService.getUsers();
    }

    @PostMapping
    public User logUser(String email, String password) throws Exception {
        User user = loginService.checkMail(email);
        String hash = user.getPassword();
        if (bCrypt.matches(password, hash)) {
            return user;
        } else {
            throw new Exception("Identifiants incorrectes");
        }
    }
}
