package fr.skyward.server.api;

import fr.skyward.server.api.wrapper.MaintenanceEndWrapper;
import fr.skyward.server.model.Maintenance;
import fr.skyward.server.service.MaintenanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "api/maintenance")
public class MaintenanceController {

    private final MaintenanceService maintenanceService;

    @Autowired
    public MaintenanceController(MaintenanceService maintenanceService) {
        this.maintenanceService = maintenanceService;
    }

    @GetMapping
    public Iterable<Maintenance> getMaintenances() {
        return maintenanceService.getMaintenances();
    }

    @PostMapping
    public void addMaintenance(@RequestBody Maintenance maintenance) {
        maintenanceService.saveMaintenance(maintenance);
    }

    @GetMapping("{id}")
    public Optional<Maintenance> getMaintenance(@PathVariable int id) {
        return maintenanceService.getMaintenance(id);
    }

    @DeleteMapping("{id}")
    public void deleteMaintenance(@PathVariable int id) {
        maintenanceService.deleteMaintenance(id);
    }

    @PostMapping("end/{id}")
    public void deleteMaintenance(@PathVariable int id, @RequestBody MaintenanceEndWrapper maintenanceEndWrapper) throws Exception {
        maintenanceService.setCloseMaintenance(id, maintenanceEndWrapper);
    }

    @PutMapping("end/{id}")
    public void modifyMaintenance(@PathVariable int id, @RequestBody boolean ended) throws Exception {
        maintenanceService.setEnded(id, ended);
    }

    @GetMapping("user/{id}")
    public Iterable<Maintenance> getMaintenanceUser(@PathVariable int id) {
        return maintenanceService.getMaintenances(id);
    }

    @PostMapping("user/{id}")
    public void setMaintenanceTechnician(@PathVariable int id, @RequestBody int userId) {
        maintenanceService.setMaintenanceTechnician(id, userId);
    }

    @DeleteMapping("user/{id}")
    public void deleteMaintenanceTechnician(@PathVariable int id) {
        maintenanceService.deleteMaintenanceTechnician(id);
    }

    @GetMapping("elevator/{elevatorId}")
    public Iterable<Maintenance> getMaintenanceElevator(@PathVariable int elevatorId) {
        return maintenanceService.getMaintenancesElevator(elevatorId);
    }

    @GetMapping("todo/{userid}")
    public Iterable<Maintenance> getMaintenanceTodoUser(@PathVariable int userid) {
        return maintenanceService.getMaintenances(userid);
    }

    @GetMapping("todo")
    public Iterable<Maintenance> getMaintenancesTodo() {
        return maintenanceService.getMaintenancesNotEnded();
    }
}
