package fr.skyward.server.api;

import fr.skyward.server.model.Elevator;
import fr.skyward.server.service.ElevatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "api/elevator")
public class ElevatorController {

    private final ElevatorService elevatorService;

    @Autowired
    public ElevatorController(ElevatorService elevatorService) {
        this.elevatorService = elevatorService;
    }

    @GetMapping
    public Iterable<Elevator> getElevators() {
        return elevatorService.getElevators();
    }

    @PostMapping
    public Elevator addElevator(Elevator elevator) {
        return elevatorService.saveElevator(elevator);
    }

    @PutMapping
    public Elevator modifyElevator(Elevator elevator) {
        return elevatorService.saveElevator(elevator);
    }

    @GetMapping(path = "{id}")
    public Optional<Elevator> getElevator(@PathVariable int id) {
        return elevatorService.getElevator(id);
    }

    @DeleteMapping(path = "{id}")
    public void deleteElevator(@PathVariable int id) {
        elevatorService.deleteElevator(id);
    }
}