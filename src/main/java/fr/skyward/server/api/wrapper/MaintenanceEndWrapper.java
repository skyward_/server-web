package fr.skyward.server.api.wrapper;

import fr.skyward.server.model.Piece;

import java.util.Set;

public class MaintenanceEndWrapper {

    private String comment;
    private Set<Piece> pieces;

    public MaintenanceEndWrapper(String comment, Set<Piece> pieces) {
        this.comment = comment;
        this.pieces = pieces;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(Set<Piece> pieces) {
        this.pieces = pieces;
    }
}
