package fr.skyward.server.service;

import fr.skyward.server.dao.RankDao;
import fr.skyward.server.model.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RankService {

    private final RankDao rankDao;

    @Autowired
    public RankService(RankDao rankDao) {
        this.rankDao = rankDao;
    }

    public Iterable<Rank> getRanks() {
        return rankDao.findAll();
    }

    public Rank getRankByName(String name) {
        return rankDao.findByName(name);
    }
}
