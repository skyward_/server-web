package fr.skyward.server.service;

import fr.skyward.server.api.wrapper.MaintenanceEndWrapper;
import fr.skyward.server.dao.MaintenanceDao;
import fr.skyward.server.model.Elevator;
import fr.skyward.server.model.Maintenance;
import fr.skyward.server.model.MaintenanceType;
import fr.skyward.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class MaintenanceService {

    private final MaintenanceDao maintenanceDao;

    @Autowired
    public MaintenanceService(MaintenanceDao maintenanceDao) {
        this.maintenanceDao = maintenanceDao;
    }

    public Iterable<Maintenance> getMaintenances() {
        return maintenanceDao.findAll();
    }

    public Optional<Maintenance> getMaintenance(int id) {
        return maintenanceDao.findById(id);
    }

    public void deleteMaintenance(int id) {
        maintenanceDao.deleteById(id);
    }

    public Iterable<Maintenance> getMaintenancesElevator(int elevatorId) {
        return maintenanceDao.findTop10ByElevatorOrderByDate(new Elevator(elevatorId));
    }

    public Iterable<Maintenance> getMaintenancesEnded(int userId) {
        return maintenanceDao.findByTechnicianAndEndedFalse(new User(userId));
    }

    public Iterable<Maintenance> getMaintenances(int userId) {
        return maintenanceDao.findByTechnician(new User(userId));
    }

    public Iterable<Maintenance> getMaintenancesNotEnded() {
        return maintenanceDao.findByEndedFalse();
    }

    public Iterable<Maintenance> getMaintenanceTypeNotEnded(MaintenanceType maintenanceType) {
        return maintenanceDao.findByMaintenanceTypeAndEndedFalse(maintenanceType);
    }

    public void saveMaintenance(Maintenance maintenance) {
        maintenanceDao.save(maintenance);
    }

    public void setCloseMaintenance(int id, MaintenanceEndWrapper maintenanceEndWrapper) throws Exception {
        final Optional<Maintenance> optionalMaintenance = maintenanceDao.findById(id);
        if (optionalMaintenance.isPresent()) {
            Maintenance maintenance = optionalMaintenance.get();
            maintenance.setPieces(maintenanceEndWrapper.getPieces());
            maintenance.setCommentTechnician(maintenanceEndWrapper.getComment());
            maintenance.setDateEnded(new Date());
            maintenanceDao.save(maintenance);
        } else
            throw new Exception("Maintenance not found");
    }

    public void setEnded(int id, boolean ended) throws Exception {
        final Optional<Maintenance> optionalMaintenance = maintenanceDao.findById(id);
        if (optionalMaintenance.isPresent()) {
            Maintenance maintenance = optionalMaintenance.get();
            maintenance.setEnded(ended);
            maintenanceDao.save(maintenance);
        } else
            throw new Exception("Maintenance not found");
    }

    public void setMaintenanceTechnician(int id, int userId) {
        maintenanceDao.updateUserOnMaintenance(userId, id);
    }

    public void deleteMaintenanceTechnician(int id) {
        maintenanceDao.removeUserOnMaintenance(id);
    }
}
