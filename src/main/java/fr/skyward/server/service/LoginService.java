package fr.skyward.server.service;

import fr.skyward.server.dao.UserDao;
import fr.skyward.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private final UserDao userDao;

    @Autowired
    public LoginService(UserDao userDao) {
        this.userDao = userDao;
    }

    public Iterable<User> getUsers() {
        return userDao.findAll();
    }

    public User checkMail(String email) {
        return userDao.findByEmail(email);
    }

}
