package fr.skyward.server.service;

import fr.skyward.server.dao.ElevatorDao;
import fr.skyward.server.model.Elevator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ElevatorService {

    private final ElevatorDao elevatorDao;

    @Autowired
    public ElevatorService(ElevatorDao elevatorDao) {
        this.elevatorDao = elevatorDao;
    }

    public Iterable<Elevator> getElevators() {
        return elevatorDao.findAll();
    }

    public Optional<Elevator> getElevator(int id) {
        return elevatorDao.findById(id);
    }

    public Elevator saveElevator(Elevator elevator) {
        return elevatorDao.save(elevator);
    }

    public void deleteElevator(int id) {
        elevatorDao.deleteById(id);
    }
}
