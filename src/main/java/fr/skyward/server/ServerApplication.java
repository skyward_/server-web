package fr.skyward.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    /*@Bean
    CommandLineRunner commandLineRunner(UserDao userDao, RankDao rankDao) {
        return args -> {
            Rank rank = new Rank("Admin");
            rankDao.save(rank);
            User user = new User("root", "root");
            user.setRank(rank);
            userDao.save(user);
        };
    }*/

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
